from trytond.pool import Pool
from .report import *
from .bikop_lab_report import *

def register():
    Pool.register(
        BikopLabReport,
        module='health_bikop_lab_report',type_='report'
        )
